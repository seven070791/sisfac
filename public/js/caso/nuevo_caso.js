$(function() {
    llenar_Tipo_Atencion(Event);
    llenar_Propiedad_Intelectual(Event);
    llenar_Estados(Event);
    llenar_Red_social(Event);
    llenar_Entes_asdcritos(Event);
});


//FUNCION PARA LLENAR EL COMBO ENTES ADSCRITOS
function llenar_Entes_asdcritos(e, ente_adscrito_id) {
    e.preventDefault;
    url = "/Listar_Entes_asdcritos";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#ente-adscrito").empty();
                $("#ente-adscrito").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (ente_adscrito_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#ente-adscrito").append(
                            "<option value=" +
                            item.ente_id +
                            ">" +
                            item.ente_nombre +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.ente_id === ente_adscrito_id) {
                            $("#ente-adscrito").append(
                                "<option value=" +
                                item.ente_id +
                                " selected>" +
                                item.ente_nombre +
                                "</option>"
                            );
                        } else {
                            $("#ente-adscrito").append(
                                "<option value=" +
                                item.ente_id +
                                ">" +
                                item.ente_nombre +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        },
    });
}

//FUNCION PARA LLENAR EL COMBO DE LAS REDES SOCIALES
function llenar_Red_social(e, id) {
    e.preventDefault;
    url = "/listar_Red_Social";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#red-social").empty();
                $("#red-social").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#red-social").append(
                            "<option value=" +
                            item.red_s_id +
                            ">" +
                            item.red_s_nom +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#red-social").append(
                                "<option value=" +
                                item.red_s_id +
                                " selected>" +
                                item.red_s_nom +
                                "</option>"
                            );
                        } else {
                            $("#red-social").append(
                                "<option value=" +
                                item.red_s_id +
                                ">" +
                                item.red_s_nom +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        },
    });
}

//FUNCION PARA LLENAR EL COMBO ESTADOS
function llenar_Estados(e, id) {
    e.preventDefault;
    url = "/llenar_Estados";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#estado-caso").empty();
                $("#estado-caso").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#estado-caso").append(
                            "<option value=" +
                            item.estadoid +
                            ">" +
                            item.estadonom +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#estado-caso").append(
                                "<option value=" +
                                item.estadoid +
                                " selected>" +
                                item.estadonom +
                                "</option>"
                            );
                        } else {
                            $("#estado-caso").append(
                                "<option value=" +
                                item.estadoid +
                                ">" +
                                item.estadonom +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        },
    });
}

//FUNCION PARA LLENAR EL COMBO TIPO DE PROPIEDAD INTELECTUAL
function llenar_Propiedad_Intelectual(e, id) {
    e.preventDefault;
    url = "/Listar_Propiedad_Intelectual";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#tipo-pi").empty();
                $("#tipo-pi").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#tipo-pi").append(
                            "<option value=" +
                            item.tipo_prop_id +
                            ">" +
                            item.tipo_prop_nombre +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#tipo-pi").append(
                                "<option value=" +
                                item.tipo_prop_id +
                                " selected>" +
                                item.tipo_prop_nombre +
                                "</option>"
                            );
                        } else {
                            $("#tipo-pi").append(
                                "<option value=" +
                                item.tipo_prop_id +
                                ">" +
                                item.tipo_prop_nombre +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        },
    });
}

//FUNCION PARA LLENAR EL COMBO TIPO DE ATENCION USUARIO
function llenar_Tipo_Atencion(e, id) {
    e.preventDefault;
    url = "/Listar_Tipo_Atencion_filtro";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#tipo-atencion-usu").empty();
                $("#tipo-atencion-usu").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#tipo-atencion-usu").append(
                            "<option value=" +
                            item.tipo_aten_id +
                            ">" +
                            item.tipo_aten_nombre +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#tipo-atencion-usu").append(
                                "<option value=" +
                                item.tipo_aten_id +
                                " selected>" +
                                item.tipo_aten_nombre +
                                "</option>"
                            );
                        } else {
                            $("#tipo-atencion-usu").append(
                                "<option value=" +
                                item.tipo_aten_id +
                                ">" +
                                item.tipo_aten_nombre +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        },
    });
}
//Evento que busca los municipios por estados
$(document).on("click", "#estado-caso", (e) => {
    e.preventDefault();

    let datos = {
        id_estado: $("#estado-caso").val(),
    };
    $.ajax({
            url: "/municipios",
            method: "POST",
            dataType: "JSON",
            data: {
                data: btoa(JSON.stringify(datos)),
            },
        })
        .then((response) => {
            $("#municipio-caso").html(response.data);

            let mun = $("#municipio-caso").val();

            if (mun != 0) {
                let datos = {
                    id_municipio: $("#municipio-caso").val(),
                };
                $.ajax({
                        url: "/parroquias",
                        method: "POST",
                        dataType: "JSON",
                        data: {
                            data: btoa(JSON.stringify(datos)),
                        },
                    })
                    .then((response) => {
                        $("#parroquia-caso").html(response.data);
                    })
                    .catch((request) => {
                        Swal.fire("Error", response.JSONmessage, "Error");
                    });
            }
        })
        .catch((request) => {
            Swal.fire("Error", response.JSONmessage, "Error");
        });
});
//Evento que busca las parroquias por municipio
$(document).on("click", "#municipio-caso", (e) => {
    e.preventDefault();
    let datos = {
        id_municipio: $("#municipio-caso").val(),
    };
    $.ajax({
            url: "/parroquias",
            method: "POST",
            dataType: "JSON",
            data: {
                data: btoa(JSON.stringify(datos)),
            },
        })
        .then((response) => {
            $("#parroquia-caso").html(response.data);
        })
        .catch((request) => {
            Swal.fire("Error", response.JSONmessage, "Error");
        });
});

$("#red-social").on('change', function() {
    $("#red-social").removeClass('is-invalid');
});

$("#estado-caso").on('change', function() {
    $("#estado-caso").removeClass('is-invalid');
});
$("#tipo-pi").on('change', function() {
    $("#tipo-pi").removeClass('is-invalid');

});
$("#tipo-atencion-usu").on('change', function() {
    $("#tipo-atencion-usu").removeClass('is-invalid');
    let tipo_atencion_usu = $("#tipo-atencion-usu").val();
    if (tipo_atencion_usu == 1) {
        $("#cgr").show();
        $("#denuncias").hide();
    } else if (tipo_atencion_usu == 5) {
        $("#cgr").hide();
        $("#denuncias").show();
    } else {
        $("#cgr").hide();
        $("#denuncias").hide();
    }

});
$("#requerimiento-usuario").on('change', function() {
    $("#requerimiento-usuario").removeClass('is-invalid');
});
//Evento de envio del formulario
$(document).on("submit", "#new-case", function(e) {
    e.preventDefault();
    let tipo_prop_intelec = $("#tipo-pi").val();
    let tipo_atencion = $("#tipo-atencion-usu").val();
    let requerimiento_user = $("#requerimiento-usuario").val();
    let red_social = $("#red-social").val();
    let estado = $("#estado-caso").val();
    let sexo = $("#sexo").val();
    requerimiento_user = requerimiento_user.trim();
    if (red_social == null) {
        $("#red-social").addClass('is-invalid');

        Swal.fire({
            icon: "success",
            type: 'error',
            html: '<strong>DEBE SELECCIONAR LA VIA DE ATENCION.</strong>',

            toast: true,
            position: "center",
            showConfirmButton: false,
            timer: 3500,
        });
    } else if (estado == null) {
        $("#red-social").removeClass('is-invalid');
        $("#estado-caso").addClass('is-invalid');
        Swal.fire({
            icon: "success",
            type: 'error',
            html: '<strong>EL CAMPO ESTADO ES OBLIGATORIO.</strong>',
            toast: true,
            position: "center",
            showConfirmButton: false,
            timer: 3500,
        });
    } else if (tipo_prop_intelec == null) {
        $("#estado-caso").removeClass('is-invalid');
        $("#tipo-pi").addClass('is-invalid');
        Swal.fire({
            icon: "success",
            type: 'error',
            html: '<strong>DEBE SELECCIONAR UN TIPO DE PROPIEDAD INTELECTUAL.</strong>',
            toast: true,
            position: "center",
            showConfirmButton: false,
            timer: 3500,
        });

    } else if (tipo_atencion == null) {
        $("#tipo-pi").removeClass('is-invalid');
        $("#tipo-atencion-usu").addClass('is-invalid');
        Swal.fire({
            icon: "success",
            type: 'error',
            html: '<strong>EL USUARIO DEBE TENER ALGUN TIPO DE ATENCION</strong>',
            toast: true,
            position: "center",
            showConfirmButton: false,
            timer: 3500,
        })
    } else if (requerimiento_user == '') {
        $("#tipo-atencion-usu").removeClass('is-invalid');
        $("#requerimiento-usuario").addClass('is-invalid');
        $
        Swal.fire({
            icon: "success",
            type: 'error',
            html: '<strong>DEBE INDICAR LA DESCRIPCION DEL CASO .</strong>',
            toast: true,
            position: "center",
            showConfirmButton: false,
            timer: 3500,
        });
    } else {
        $("#red-social").removeClass('is-invalid');
        $("#estado-caso").removeClass('is-invalid');
        $("#tipo-pi").removeClass('is-invalid');
        $("#tipo-atencion-usu").removeClass('is-invalid');
        $("#requerimiento-usuario").removeClass('is-invalid');
        //VERIFICO SI LA ATENCION ES ASESORIA PARA TOMAR EL VALOR DE LOS CAMPOS CORREPONDIENTES
        let tipo_atencion_usu = $("#tipo-atencion-usu").val();
        //VARIABLES PARA CGR
        let competencia_crg
        let asume_crg
        let ente_adscrito
        let bandera_cgr = false;
        //VARIABLES PARA LA DEDUNCIA
        let option_personal
        let option_comunidad
        let option_terceros
        let bandera_denuncia = false;
        let fecha_hechos = $('#fecha-hechos').val();
        let denu_involucrados = $('#denu-involucrados').val();
        let nombre_instancia = $('#nombre-instancia').val();
        let rif_instancia = $('#rif-instancia').val();
        let ente_financiador = $('#ente-financiador').val();
        let nombre_proyecto = $('#nombre-proyecto').val();
        let monto_aprovado = $('#monto-aprovado').val();
        denu_involucrados = denu_involucrados.trim();
        if (tipo_atencion === '1') {
            competencia_crg = $("#competencia-cgr").val()
            asume_crg = $("#asume-cgr").val()
            if (competencia_crg == null) {
                alert('DEBE INDICAR SI APLICA O NO  LA COMPETENCIA DEL CGR')
            } else if (asume_crg == null) {
                alert('DEBE INDICAR SI ASUME CGR')
            } else {
                bandera_cgr = true;
                valor_competencia = $("#competencia-cgr").val();
                valor_asume = $("#asume-cgr").val();
                let datos = {
                    "social_network": $("#red-social").val(),
                    "date-entry": $("#fecha-recibido").val(),
                    "person-name": $("#nombre-persona").val(),
                    "person-lastname": $("#apellido-persona").val(),
                    "person-id": $("#cedula-persona").val(),
                    "nacionalidad": $("#tipo-persona").val(),
                    "telephone": $("#telefono").val(),
                    "country": $("#pais-caso").val(),
                    "state": $("#estado-caso").val(),
                    "county": $("#municipio-caso").val(),
                    "town": $("#parroquia-caso").val(),
                    "bandera_denuncia": bandera_denuncia,
                    "record-work": $("#num-tramite").val(),
                    "pi-type": $("#tipo-pi").val(),
                    "user-requirement": $("#requerimiento-usuario").val(),
                    "office": $("#office").val(),
                    "tipo-atencion-usu": $("#tipo-atencion-usu").val(),
                    "sexo": $("#sexo").val(),
                    "bandera_cgr": bandera_cgr,
                    "competencia_crg": competencia_crg,
                    "asume_crg": asume_crg,
                    "tipo_beneficiario": $("#t-beneficiario").val(),
                    "direccion": $("#direccion").val(),
                    "correo": $("#correo").val(),
                    "ente_adscrito": $("#ente-adscrito").val(),
                }
                $.ajax({
                    url: "/registrarCaso",
                    method: "POST",
                    dataType: "JSON",
                    data: {
                        "data": btoa(JSON.stringify(datos))
                    },
                    beforeSend: function() {
                        //$("button[type=submit]").attr('disabled', 'true');
                    },
                    success: function(respuesta) {

                        if (respuesta.mensaje === 1) {
                            Swal.fire({
                                icon: "success",
                                type: 'success',
                                html: '<strong>Caso registrado exitosamente con el Nª' + ' ' + ' ' + respuesta.idcaso + '</strong>',
                                toast: true,
                                position: "center",
                                showConfirmButton: false,
                                //timer: 3500,
                            });
                            setTimeout(function() {
                                window.location = "/casos";
                            }, 1500);
                        } else if (respuesta.mensaje === 2) {
                            Swal.fire({
                                icon: "error",
                                type: 'error',
                                html: '<strong>Hubo un error en el registro del requerimiento del usuario .</strong>',
                                toast: true,
                                position: "center",
                                showConfirmButton: false,
                                //timer: 3000,
                            });
                            setTimeout(function() {
                                window.location = "/casos";
                            }, 1500);
                        }
                    }
                });
            }
        } else if (tipo_atencion === '5') {
            if (document.getElementById('option-personal').checked) {
                option_personal = true
            } else {
                option_personal = false
            }
            if (document.getElementById('option-comunidad').checked) {
                option_comunidad = true
            } else {
                option_comunidad = false
            }
            if (document.getElementById('option-terceros').checked) {
                option_terceros = true
            } else {
                option_terceros = false
            }

            if (option_personal == false && option_comunidad == false && option_terceros == false) {
                alert('Debe indicar a quien afecta el hecho');
            } else {
                if (fecha_hechos == '') {
                    alert('Debe selecciar la fecha en que ocurrieron los hechos');

                } else if (denu_involucrados === '') {
                    $("#denu-involucrados").addClass('is-invalid');
                    alert('Este campo es requerido , por favor introduzca la informacion solicitada');
                } else {
                    bandera_denuncia = true;
                    $("#denu-involucrados").removeClass('is-invalid');
                    let datos = {
                        "social_network": $("#red-social").val(),
                        "date-entry": $("#fecha-recibido").val(),
                        "person-name": $("#nombre-persona").val(),
                        "person-lastname": $("#apellido-persona").val(),
                        "person-id": $("#cedula-persona").val(),
                        "nacionalidad": $("#tipo-persona").val(),
                        "telephone": $("#telefono").val(),
                        "country": $("#pais-caso").val(),
                        "state": $("#estado-caso").val(),
                        "county": $("#municipio-caso").val(),
                        "town": $("#parroquia-caso").val(),
                        "record-work": $("#num-tramite").val(),
                        "pi-type": $("#tipo-pi").val(),
                        "user-requirement": $("#requerimiento-usuario").val(),
                        "office": $("#office").val(),
                        "tipo-atencion-usu": $("#tipo-atencion-usu").val(),
                        "sexo": $("#sexo").val(),
                        "bandera_denuncia": bandera_denuncia,
                        "option_personal": option_personal,
                        "option_comunidad": option_comunidad,
                        "option_terceros": option_terceros,
                        "fecha_hechos": fecha_hechos,
                        "denu_involucrados": denu_involucrados,
                        "nombre_instancia": nombre_instancia,
                        "rif_instancia": rif_instancia,
                        "ente_financiador": ente_financiador,
                        "nombre_proyecto": nombre_proyecto,
                        "monto_aprovado": monto_aprovado,
                        "bandera_cgr": bandera_cgr,
                        "tipo_beneficiario": $("#t-beneficiario").val(),
                        "direccion": $("#direccion").val(),
                        "correo": $("#correo").val(),
                    }
                    $.ajax({
                        url: "/registrarCaso",
                        method: "POST",
                        dataType: "JSON",
                        data: {
                            "data": btoa(JSON.stringify(datos))
                        },
                        beforeSend: function() {
                            //$("button[type=submit]").attr('disabled', 'true');
                        },
                        success: function(respuesta) {

                            if (respuesta.mensaje === 1) {
                                Swal.fire({
                                    icon: "success",
                                    type: 'success',
                                    html: '<strong>Caso registrado exitosamente con el Nª' + ' ' + ' ' + respuesta.idcaso + '</strong>',
                                    toast: true,
                                    position: "center",
                                    showConfirmButton: false,
                                    //timer: 3500,
                                });
                                setTimeout(function() {
                                    window.location = "/casos";
                                }, 1500);
                            } else if (respuesta.mensaje === 2) {
                                Swal.fire({
                                    icon: "error",
                                    type: 'error',
                                    html: '<strong>Hubo un error en el registro del requerimiento del usuario .</strong>',
                                    toast: true,
                                    position: "center",
                                    showConfirmButton: false,
                                    //timer: 3000,
                                });
                                setTimeout(function() {
                                    window.location = "/casos";
                                }, 1500);
                            }
                        }
                    });
                }

            }

        } else {
            bandera_cgr = false;
            bandera_denuncia = false;
            valor_competencia = '';
            ente_adscrito = 0
            valor_asume = '';
            let datos = {
                "social_network": $("#red-social").val(),
                "date-entry": $("#fecha-recibido").val(),
                "person-name": $("#nombre-persona").val(),
                "person-lastname": $("#apellido-persona").val(),
                "person-id": $("#cedula-persona").val(),
                "nacionalidad": $("#tipo-persona").val(),
                "telephone": $("#telefono").val(),
                "country": $("#pais-caso").val(),
                "state": $("#estado-caso").val(),
                "county": $("#municipio-caso").val(),
                "town": $("#parroquia-caso").val(),
                "record-work": $("#num-tramite").val(),
                "pi-type": $("#tipo-pi").val(),
                "user-requirement": $("#requerimiento-usuario").val(),
                "office": $("#office").val(),
                "tipo-atencion-usu": $("#tipo-atencion-usu").val(),
                "sexo": $("#sexo").val(),
                "bandera_cgr": bandera_cgr,
                "bandera_denuncia": bandera_denuncia,
                "competencia_crg": competencia_crg,
                "ente_adscrito": ente_adscrito,
                "asume_crg": asume_crg,
                "tipo_beneficiario": $("#t-beneficiario").val(),
                "direccion": $("#direccion").val(),
                "correo": $("#correo").val(),
            }
            $.ajax({
                url: "/registrarCaso",
                method: "POST",
                dataType: "JSON",
                data: {
                    "data": btoa(JSON.stringify(datos))
                },
                beforeSend: function() {
                    //$("button[type=submit]").attr('disabled', 'true');
                },
                success: function(respuesta) {

                    if (respuesta.mensaje === 1) {
                        Swal.fire({
                            icon: "success",
                            type: 'success',
                            html: '<strong>Caso registrado exitosamente con el Nª' + ' ' + ' ' + respuesta.idcaso + '</strong>',
                            toast: true,
                            position: "center",
                            showConfirmButton: false,
                            //timer: 3500,

                        });
                        setTimeout(function() {
                            window.location = "/casos";
                        }, 1500);
                    } else if (respuesta.mensaje === 2) {
                        Swal.fire({
                            icon: "error",
                            type: 'error',
                            html: '<strong>Hubo un error en el registro del requerimiento del usuario .</strong>',
                            toast: true,
                            position: "center",
                            showConfirmButton: false,
                            //timer: 1500,
                        });
                        setTimeout(function() {
                            window.location = "/casos";
                        }, 1500);
                    }
                }
            });
        }

    }


});