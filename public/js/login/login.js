const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

$(document).on('submit', "#login-user", function(e) {
    e.preventDefault();
    let datos = {
        "username": $("#usuario-email").val(),
        "userpass": $("#usuario-clave").val()
    }
    $.ajax({
        url: "/signin",
        method: "GET",
        dataType: "TEXT",
        data: {
            "data": btoa(JSON.stringify(datos))
        },
        beforeSend: function() {
            //$("button[type=submit]").attr('disabled', 'true');
        },
        success: function(data) {
						//console.log(data);
            if (data == 0) {
                Toast.fire({
                    type: 'error',
                    title: "Usuario Bloqueado"
                });
            } else if (data == 1) {
                Toast.fire({
                    type: 'success',
                    title: "Iniciando Sesion"

                });
                setTimeout(function() {
                    window.location = "/pantalla_bienvenida"; //Pantalla con la presentación del sistema, que 
										 //vía .js envía a la pantalla de inicio (La inhabilité temporalmente)
                    //window.location = "/inicio";
                }, 1400);
            } else if (data == 2) {
                Toast.fire({
                    type: 'error',
                    title: "Contraseña Incorrecta"
                });
            } else if (data == 3) {
                Toast.fire({
                    type: 'error',
                    title: "Usuario o contraseña incorrectos"
                });
            }
        }
    })
});

/*Verficacion de datos en el form*/
$(document).on('change', '#usuario-email', function(e) {
    let texto = $("#usuario-email").val();
    if (texto.match(/\w*.\w*\@sapi.gob.ve/) == null) {
        $("#usuario-email").addClass('is-invalid');
        $("button[type=submit]").attr('disabled', 'true');
    } else if (texto.lenght < 5) {
        $("#usuario-email").addClass('is-invalid');
        $("button[type=submit]").attr('disabled', 'true');
    } else {
        $("#usuario-email").removeClass('is-invalid');
        $("#usuario-email").addClass('is-valid');
        $("button[type=submit]").removeAttr('disabled');
    }
});
