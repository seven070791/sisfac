/*
 *Este es el document ready
 */


let cant_usu = $('#t_usuario').val();
let cant_empr = $('#t_emprendedor').val();
var numero1 = parseInt(cant_usu);
var numero2 = parseInt(cant_empr);
var suma = numero1 + numero2;
$('#t_casos').val(suma);



$(document).on('click', '.consultar', function(e) {
    e.preventDefault();
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }
    if (desde == 'null' && hasta == 'null') {
        alert('DEBE INGRESAR EL RANGO DE FECHA ')
    } else if (desde == 'null' && hasta != 'null') {} else if (hasta == 'null' && desde != 'null') {
        alert('DEDE INDICAR EL CAMPO HASTA');
    } else if (hasta < desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
    } else {

        // $('#fecha_desde').val(desde);
        // $('#fecha_hasta').val(hasta);
        window.location = "/estadisticas_con_filtro/" + desde + '/' + hasta;
    }
})

$(document).on('click', '.limpiar', function(e) {
    e.preventDefault();

    window.location = "/estadisticas";

})



$("#downloadPdf").click(function(event) {
    // get size of report page

    var reportPageHeight = $("#reportPage").innerHeight();
    var reportPageWidth = $("#reportPage").innerWidth();

    // create a new canvas object that we will populate with all other canvas objects
    var pdfCanvas = $("<canvas />").attr({
        id: "canvaspdf",
        width: reportPageWidth,
        height: reportPageHeight
    });

    // keep track canvas position
    var pdfctx = $(pdfCanvas)[0].getContext("2d");
    var pdfctxX = 50;
    var pdfctxY = 110;
    var buffer = 50;

    // for each chart.js chart
    $("canvas").each(function(index) {
        // get the chart height/width
        var canvasHeight = $(this).innerHeight();
        var canvasWidth = $(this).innerWidth();

        // draw the chart into the new canvas
        pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
        pdfctxX += canvasWidth + buffer;

        // our report page is in a grid pattern so replicate that in the new canvas


        if (index % 2 === 1) {
            pdfctxX = 0;
            pdfctxY += canvasHeight + buffer;
        }
    });

    // create new pdf and add our new canvas as an image
    var pdf = new jsPDF("legal", "pt", "L" [reportPageWidth, reportPageHeight]);
    pdf.format = [10, 15]
    pdf.addImage($(pdfCanvas)[0], "PNG", 0, 0, );
    let imageData = rootpath;
    pdf.addImage(imageData, "PNG", 25, 5, 800, 45);





    var cant_usuario = document.getElementById('t_usuario').value;
    var cant_emprendedor = document.getElementById('t_emprendedor').value;
    var casoswhatsaapp = document.getElementById('casoswhatsaapp').value;
    var casospersonal = document.getElementById('casospersonal').value;
    var casosllamadas = document.getElementById('casosllamadas').value;
    var casoscorreo = document.getElementById('casoscorreo').value;
    var casosmarcas = document.getElementById('casosmarcas').value;
    var casospatentes = document.getElementById('casospatentes').value;
    var casosderechoautor = document.getElementById('casosderechoautor').value;
    var casosindicaciones = document.getElementById('casosindicaciones').value;
    var suma = document.getElementById('t_casos').value;
    let fecha_desde = document.getElementById('fecha_desde').value;
    let fecha_hasta = document.getElementById('fecha_hasta').value;



    pdf.setFontSize(12);

    pdf.text(50, 295, 'Casos Atendidos por Whatsapp: ' + casoswhatsaapp);
    pdf.text(50, 315, 'Casos Atendidos de forma personal: ' + casospersonal);
    pdf.text(50, 335, 'Casos Atendidos por llamada: ' + casosllamadas);
    pdf.text(50, 355, 'Casos Atendidos por correo: ' + casoscorreo);
    pdf.text(470, 295, 'Casos Atendidos por Marcas: ' + casosmarcas);
    pdf.text(470, 315, 'Casos Atendidos por Patentes: ' + casospatentes);
    pdf.text(470, 335, 'Casos por Derecho de Autor: ' + casosderechoautor);
    pdf.text(470, 355, 'Casos por Indi Geograficas: ' + casosindicaciones);
    pdf.text(50, 100, ' Casos por Usuarios: ' + cant_usuario);
    pdf.text(50, 70, ' Desde: ' + fecha_desde);
    pdf.text(250, 70, ' Hasta: ' + fecha_hasta);
    pdf.text(250, 100, ' Casos por Emprendedores: ' + cant_emprendedor);
    pdf.text(450, 100, 'Total casos : ' + suma);
    // download the pdf
    pdf.save("filename.pdf");
});