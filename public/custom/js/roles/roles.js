/*
 *Este es el document ready
 */
$(function()
//$(document).on("ready",'', function(e)
{
     listarRoles();
});
/*
 * Función para definir datatable:
 */
function listarRoles()
{
     $('#table_roles').DataTable
     (
	  {
	       "order":[[0,"asc"]],					
	       "paging":true,
	       "info":true,
	       "filter":true,
	       "responsive": true,
	       "autoWidth": true,					
	       //"stateSave":true,
	       "ajax":
	       {
		    "url":"/listar_roles",
		    "type":"GET",
		    dataSrc:''
	       },
	       "columns":
	       [
		       {data:'id'},
		       {data:'rol'},
		       {data:'estatus'},
		       {orderable: true,
			    render:function(data, type, row)
			    {
			     return '<a href="javascript:;" class="btn btn-info editar-rol" data-toggle="tooltip" title="Actualizar Descripción y Estatus del Rol" id='+row.id+' rol='+row.rol+' estatus="'+row.estatus+'">Editar</a>'+'<a href="javascript:;" class="btn btn-info perfil-rol" data-toggle="tooltip" title="Actualizar Perfil del Rol" id='+row.id+' rol='+row.rol+' estatus="'+row.estatus+'">Perfil</a>'
			       }
		       }
	       ],
	       "language":
	       {
		    "sProcessing":    "Procesando...",
		    "sLengthMenu":    "Mostrar _MENU_ registros",
		    "sZeroRecords":   "No se encontraron resultados",
		    "sEmptyTable":    "Ningún dato disponible en esta tabla",
		    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":   "",
		    "sSearch":        "Buscar:",
		    "sUrl":           "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": 
		    {
			 "sFirst":    "Primero",
			 "sLast":    "Último",
			 "sNext":    "Siguiente",
			 "sPrevious": "Anterior"
		    },
		    "oAria":
		    {
			 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    },
		    "columnDefs": 
		    [
		       {
				"targets": [ 0 ],
				"visible": false,
				"searchable": false
			}
		    ],			
	       }
	  });
}
// Reposicionar Esto si sirve //
$('#lista_de_roles').on('click','.perfil-rol', function(e)
{
     e.preventDefault();
     id=$(this).attr('id');
     rol=$(this).attr('rol');
     window.location='/definirPerfil/'+id+'/'+rol;
});
$('#lista_de_roles').on('click','.editar-rol', function(e)
{
     e.preventDefault();
     id=$(this).attr('id');
     if(confirm('Editar Descripción del Rol?'))
     {
	  var data=
	  {
	       aide:id
	  }
	  $.ajax
	  ({
	       url:'/datosRol',
	       method:'GET',
	       data:{data:btoa(JSON.stringify(data))},
	       dataType:'JSON',
	       beforeSend:function(data)
	       {
	       },
	       success:function(data)
	       {
		    $('#ventanaModalRegistrarRol').modal('show');
		    $('#ventanaModalRegistrarRol').find('#btnActualizarRol').show();
		    $('#ventanaModalRegistrarRol').find('#btnGuardarRol').hide();
		    $('#ventanaModalRegistrarRol').find('#txtNombreRol').val(data.rol);
		    $('#ventanaModalRegistrarRol').find('#txtIde').val(data.id);
		    //$('#txtNombreRol').val(data.rol);
		    //$('#txtIde').val(data.id);
		    if(data.activo=='t')
		    {
			 $('#ventanaModalRegistrarRol').find('#chkActivo').attr('checked','checked');
		    }
		    else
		    {
			 $('#ventanaModalRegistrarRol').find('#chkActivo').removeAttr('checked');
		    }
	       },
	       error:function(xhr, status, errorThrown)
	       {
		    alert(xhr.status);
		    alert(errorThrown);
	       }
	  });
     }
});
//$('#btnCrearRol').on('click', function(e)
$(document).on('click','#btnCrearRol', function(e)
{
     $('#ventanaModalRegistrarRol').modal('show');
     $('#ventanaModalRegistrarRol').find('.modal-title').text('Crear Rol de Usuario');
     $('#ventanaModalRegistrarRol').find('#btnActualizarRol').hide();
     $('#ventanaModalRegistrarRol').find('#btnGuardarRol').show();
});
$(document).on('click','#btnGuardarRol', function(e)
{
     e.preventDefault();
     var nombreRol=$('#txtNombreRol').val();
     var activo               ='true';

     if($("#chkActivo").is(':checked'))
     {
	  activo="true";
     }
     else
     {
	  activo="false";
     }

     nombre=verificarcampolleno(e, nombreRol);
     if(nombre==0)
     {
	  alerta('información en el campo Rol','#txtNombreRol');
     }
     else
     {
	  ingresarRol(e, nombreRol, activo)
     }
});
/*
 * Funcion para verificar que el campo del formulario no este en blanco
 */
function verificarcampolleno(e, campo_a_verificar)
{
     e.preventDefault();
     var campo=campo_a_verificar
     if(campo==='')
     {
	  return 0;
     }
     else
     {
	  return 1;
     }
}
/*
*Función para emitir las alertas al validar el contenido
*de los campos del formulario
*/
function alerta(mensaje, campo)
{
     var mensaje = mensaje;
     var campo   = campo;
     $('.alert-warning').html('Debe suministrar '+ mensaje).fadeIn().delay(2000).fadeOut('slow');
     $(campo).focus();
}
/*
 * Función para Ingresar Registros en la tabla de roles
*/
function ingresarRol(e, rol_a_incluir, activo)
{
     e.preventDefault();
     var data=
     {
	  rol    :rol_a_incluir,
	  activo :activo,
     }
     $.ajax
     ({
	  url:'/agregarRol',
	  method:'POST',
	  data:{data:btoa(JSON.stringify(data))},
	  dataType:'JSON',
	  beforeSend:function(data)
	  {
	  },
	  success:function(data)
	  {
	       if(data=='1')
	       {
		    alert('Rol Incorporado');
		    window.location = '/vistaRoles';
	       }
	       else
	       {
		    alert('Error en la Incorporación del Rol');
	       }
	  },
	  error:function(xhr, status, errorThrown)
	  {
	       alert(xhr.status);
	       alert(errorThrown);
	  }
     });
}
$(document).on('click','#btnActualizarRol', function(e)
{
     e.preventDefault();
     var id       =$('#txtIde').val();
     var nombreRol=$('#txtNombreRol').val();
     var activo               ='true';

     if($("#chkActivo").is(':checked'))
     {
	  activo="true";
     }
     else
     {
	  activo="false";
     }
     nombre=verificarcampolleno(e, nombreRol);
     if(nombre==0)
     {
	  alerta('información en el campo Rol','#txtNombreRol');
     }
     else
     {
	  actualizarRol(e, id, nombreRol, activo)
     }
});
/*
 * Función para Actualizar Registro del rol
*/
function actualizarRol(e, id_a_actualizar, rol_a_actualizar, activo)
{
     e.preventDefault();
     var data=
     {
	  aide   :id_a_actualizar,
	  rol    :rol_a_actualizar,
	  activo :activo,
     }
     $.ajax
     ({
	  url:'/actualizarRol',
	  method:'POST',
	  data:{data:btoa(JSON.stringify(data))},
	  dataType:'JSON',
	  beforeSend:function(data)
	  {
	       alert('Procesando Información ...');
	  },
	  success:function(data)
	  {
	       //alert(data);
	       if(data=='1')
	       {
		    alert('Rol Actualizado');
		    window.location = '/vistaRoles';
	       }
	       else
	       {
		    alert('Error en la Incorporación del usuario');
	       }
	  },
	  error:function(xhr, status, errorThrown)
	  {
	       alert(xhr.status);
	       alert(errorThrown);
	  }
     });
}
