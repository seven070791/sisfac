<style type="text/css">
#table_roles
{
    table-layout: auto;
}
#table_roles td
{
    text-overflow: Ellipsis;
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Tablero</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <!-- container-fluid -->
    <div class="container-fluid">
      <!-- /.row -->
      <div class="row">
        <section class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">
               <div class="row col-md-12">
	       <div class="row col-md-12">
		    <h3>Listado de Roles</h3>
	       </div>
		   <button id="btnCrearRol" class="btn btn-success">Crear Rol</button><br /><br />
	</div>

	<div class="alert alert-success" style="display: none;"></div>
	<div class="alert alert-danger" style="display: none;"></div>	

              </h3>
            </div>
            <div class="card-body">
			<table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px">
                <thead>
                  <tr>
                  <td>Id</td>
				<td>Rol</td>
				<td>Estatus</td>
				<td>Acciones</td>
                  </tr>
                </thead>
                <tbody id="requisiciones">
                </tbody>
              </table>
            </div>
          </div>
        </section>
      </div>
 

    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- Ventana Modal Para Registrar Rol -->
<div id="ventanaModalRegistrarRol" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title"></h1>
      </div>
      <div class="modal-body">
        <form id="usuarioFormulario" action="" method="post" class="form-horizontal">
	     <div class="alert alert-warning" style="display: none;"></div>
        	<input type="hidden" name="txtIde" id="txtIde" value="0">
        	<div class="form-group">
        		<label for="txtNombreRol" class="label-control col-md-5">Rol :</label>
        		<div class="col-md-8">
        			<input type="text" name="txtNombreRol" id="txtNombreRol" class="form-control" maxlength="30">
        		</div>        		
        	</div>
	       <div class="form-group">
		    <div class="col-md-8">
			 <div class="form-check">
			   <input class="form-check-input" type="checkbox" name="chkActivo" id="chkActivo" value="" checked>
			   <label class="form-check-label" for="activo">
			     &nbsp;&nbsp;&nbsp;&nbsp;Activo
			   </label>
			 </div><br>
		    </div>
	       </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnGuardarRol">Guardar</button>
        <button type="button" class="btn btn-primary" id="btnActualizarRol">Actualizar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Fin Ventana Modal Para Registrar Carnéts -->
</div>

  <!-- /.content -->
</div>

