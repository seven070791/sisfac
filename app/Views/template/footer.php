<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <div class="float-right d-none d-sm-inline">
    Creado por <b>implementar tabla ...</b>
  </div>
  <!-- Default to the left -->
  <strong>Copyleft &copy; 2027578 <a href="http://sapi.gob.ve">Servicio Autónomo de la Propiedad Intelectual</a>.</strong>
</footer>
</div>

<script src="<?php echo base_url(); ?>/theme/plugins/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>/js/adminlte.js"></script>
<script src = "<?php echo base_url(); ?>/theme/plugins/bootstrap/js/bootstrap.bundle.min.js" ></script>

<script src="<?php echo base_url(); ?>/theme/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?php echo base_url(); ?>/theme/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>/theme/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/theme/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>/theme/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="<?php echo base_url(); ?>/js/core.js"></script>
