<?php
$session = session();
?>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo base_url(); ?>/css/navar.css">
<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-whiFte navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>
      <input type="text" name="" disabled="disabled" style="width: 410px; background-color: transparent; border: none;" value="">
      <h5><span class="brand-text" style="display: flex; justify-content: center; align-items: center; color: white; font-weight: bold;">SISFAC</span></h5>
      <ul class="navbar-nav ml-auto  ">
        <li class="nav-item">
        <li class="nav-item d-none d-sm-inline-block">
          <h5><a href="#Foo" onclick="cerrarSesion();" style="color: white;" class="nav-primary">Salir</a></h5>
        </li>
        <script>
          function cerrarSesion() {
            Swal.fire({
              title: '¿Deseas salir?',
              text: 'Estás a punto de abandonar la página.',
              icon: 'question',
              showCancelButton: true,
              confirmButtonText: 'Sí',
              cancelButtonText: 'No'
            }).then((result) => {
              if (result.value) {
                // Aquí puedes agregar la lógica para salir de la página
                window.location.href = '/';
              }
            });
          }
        </script>
        </li>
      </ul>
    </nav>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: linear-gradient(to right, #263846, #344958, #263846);">
      <!-- Brand Logo -->

      <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">SISFAC </span>
        <img src="<?php echo base_url(); ?>/img/LogoSIAC_sapi2.png" alt="AdminLTE Logo" class="brand-image" style="opacity: .8; float: left; box-shadow: none; width: 50px;" onclick="return false;">
      </a>
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="info" style="text-align: center;">
            
          <h6 style="color: white"><?= session('nombre'); ?></h6>
            
          </div>
        </div>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu1">
          <li class="nav-item ">
            <a href="<?php echo base_url(); ?>/inicio" class="nav-link "><i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Inicio </p>
            </a>
          </li>
					<?php if(!empty($matrizMenu0)): ?>
							 <?php foreach($matrizMenu0 as $menu0): ?>
								 <li class="nav-item">
									 <a href="<?=$menu0["ruta"]; ?>" class="nav-link" id="">
										<i class="nav-icon fas fa-copy" id="menu_citas"></i>
										 <p><?=$menu0["menu_0"]; ?></p>
										 <i class="right fas fa-angle-left"></i>
									 </a>
										<?php if(!empty($matrizMenu1)): ?>
												 <ul class="nav nav-treeview">
															<?php foreach($matrizMenu1 as $menu1): ?>
																	 <?php if($menu1["id_menu_0"]==$menu0["id"]) : ?>
																				<li class="nav-item">
																						 <!--a href=<?= base_url() . $menu1["ruta"]; ?> class="nav-link" id="casos"-->
																						 <a href=<?= base_url() . $menu1["ruta"]; ?> class="nav-link" id="casos">
																						 <i class="nav-icon fas fa-copy" id="menu_citas"></i>
																						 <p><?=$menu1["menu_1"]; ?></p>
																						 <i class="right fas fa-angle-left"></i>
																						 </a>
																						 <?php if(!empty($matrizMenu2)): ?>
																									<ul class="nav nav-tree">
																											 <?php foreach($matrizMenu2 as $menu2): ?>
																														<?php if($menu2["id_menu_1"]==$menu1["id"]) : ?>
																																 <li class="nav-item">
																																			<a href="<?=$menu2["ruta"]; ?>" class="nav-link" id="casos">
																																			<i class="nav-icon fas fa-copy" id="menu_citas"></i>
																																			<p><?=$menu2["menu_2"]; ?></p>
																																			<i class="right fas fa-angle-left"></i>
																																			</a>
																																			<?php if(!empty($matrizMenu3)): ?>
																																					 <ul class="nav nav-tree">
																																								<?php foreach($matrizMenu3 as $menu3): ?>
																																										 <?php if($menu3["id_menu_2"]==$menu2["id"]) : ?>
																																													<li class="nav-item">
																																															 <a href="<?=$menu3["ruta"]; ?>" class="nav-link" id="casos">
																																															 <i class="nav-icon fas fa-copy" id="menu_citas"></i>
																																															 <p><?=$menu3["menu_3"]; ?></p>
																																															 <i class="right fas fa-angle-left"></i>
																																															 </a>
																																													</li>
																																										 <?php endif; ?>
																																								<?php endforeach; ?>
																																					 </ul>
																																			<?php endif; ?>
																																 </li>
																														<?php endif; ?>
																											 <?php endforeach; ?>
																									</ul>
																						 <?php endif; ?>
																				</li>
																	 <?php endif; ?>
															<?php endforeach; ?>
												 </ul>
										<?php endif; ?>
								 </li>
							 <?php endforeach; ?>
					<?php endif; ?>
          <li class="nav-item">
            <a href="#Foo" onclick="cerrarSesion();" class="nav-link" id="casos"><i class="fa fa-external-link" aria-hidden="true"></i>
              <p>Salir</p>

            </a>
          </li>
</ul>
      </div>
    </aside>
</body>
