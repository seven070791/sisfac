<?php namespace App\Models;
class Roles_model extends BaseModel{

     public function getAll($estatus=null)
     {
	  $builder = $this->dbconn('seguridad.rol as r');
	  $builder->select
	  (
	       "r.id
	       ,r.rol
	       ,CASE WHEN r.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus"
	  );
	  $query = $builder->where('r.id !=', 1);
	  $query = $builder->get();
	  return $query;
     }
     public function getAllParaSistemas($estatus=null)
     {
	  $builder = $this->dbconn('seguridad.rol as r');
	  $builder->select
	  (
	       "r.id
	       ,r.rol
	       ,CASE WHEN r.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus"
	  );
	  $query = $builder->get();
	  return $query;
     }
     public function Agregar($data){
	   $builder = $this->dbconn('seguridad.rol');
	   $query = $builder->insert($data);
	   return $query;
     }
     public function getDatosRol($id=null){
	  $builder = $this->dbconn('seguridad.rol r');
	  $builder->select
	       (
		    'r.id
		    ,r.rol
		    ,r.activo'
	       );
	  $builder->where('r.id', $id);
	  $query = $builder->get();
	  return $query;
     }
     public function actualizar($data){
	  $builder = $this->dbconn('seguridad.rol r');
	  $builder->where('r.id', $data['id']);
	  $query = $builder->update($data);
	  return $query;
     }
}
