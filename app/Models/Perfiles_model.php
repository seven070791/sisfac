<?php namespace App\Models;
class Perfiles_model extends BaseModel{

     public function getDatosMenu0(){
	  $builder = $this->dbconn('seguridad.menu_0 m0');
	  $builder->select
	       (
		    'm0.id,
		    ,m0.menu_0'
	       );
	  $builder->where('m0.activo', true);
	  $builder->orderBy('m0.id', 'asc');
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu0($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_0 rm0');
	  $builder->select
	       (
		    'rm0.id_menu_0'
	       );
	  $builder->where('rm0.id_rol', $id_rol);
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu0Logueado($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_0 rm0');
	  $builder->select
	       (
		    'm0.id
		    ,m0.menu_0
		    ,m0.ruta'
	       );
	  $builder->join('seguridad.menu_0 m0','rm0.id_menu_0=m0.id');
	  $builder->where('rm0.id_rol', $id_rol);
	  $builder->where('m0.activo', true);
	  $builder->orderBy('m0.orden', 'asc');
	  $query = $builder->get();
	  return $query;
     }
     public function getDatosMenu1(){
	  $builder = $this->dbconn('seguridad.menu_1 m1');
	  $builder->select
	       (
		    'm1.id,
		    ,m1.id_menu_0
		    ,m1.menu_1'
	       );
	  $builder->where('m1.activo', true);
	  $builder->orderBy('m1.id', 'asc');
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu1($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_1 rm1');
	  $builder->select
	       (
		    'rm1.id_menu_0
		    ,rm1.id_menu_1'
	       );
	  $builder->where('rm1.id_rol', $id_rol);
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu1Logueado($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_1 rm1');
	  $builder->select
	       (
		    'm1.id
		    ,m1.id_menu_0
		    ,m1.menu_1
		    ,m1.ruta'
	       );
	  $builder->join('seguridad.menu_1 m1','rm1.id_menu_1=m1.id');
	  $builder->where('rm1.id_rol', $id_rol);
	  $builder->where('m1.activo', true);
	  $builder->orderBy('m1.orden', 'asc');
	  $query = $builder->get();
	  return $query;
     }
     public function getDatosMenu2(){
	  $builder = $this->dbconn('seguridad.menu_2 m2');
	  $builder->select
	       (
		    "m2.id
		    ,m2.id_menu_1
		    ,m2.menu_2"
	       );
	  $builder->where('m2.activo', true);
	  $builder->orderBy('m2.id', 'asc');
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu2($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_2 rm2');
	  $builder->select
	       (
		    'rm1.id_menu_0
		    ,rm2.id_menu_1
		    ,rm2.id_menu_2'
	       );
	  $builder->join('seguridad.rol_menu_1 rm1','rm2.id_menu_1=rm1.id_menu_1');
	  $builder->where('rm2.id_rol', $id_rol);
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu2Logueado($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_2 rm2');
	  $builder->select
	       (
		    'm2.id
		    ,m2.id_menu_1
		    ,m2.menu_2
		    ,m2.ruta'
	       );
	  $builder->join('seguridad.menu_2 m2','rm2.id_menu_2=m2.id');
	  $builder->where('rm2.id_rol', $id_rol);
	  $builder->where('m2.activo', true);
	  $builder->orderBy('m2.orden', 'asc');
	  $query = $builder->get();
	  return $query;
     }
     public function getDatosMenu3(){
	  $builder = $this->dbconn('seguridad.menu_3 m3');
	  $builder->select
	       (
		    'm3.id
		    ,m3.id_menu_2
		    ,m3.menu_3'
	       );
	  $builder->where('m3.activo', true);
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu3($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_3 rm3');
	  $builder->select
	       (
		    'rm1.id_menu_0
		    ,rm2.id_menu_1
		    ,rm3.id_menu_2
		    ,rm3.id_menu_3'
	       );
	  $builder->join('seguridad.rol_menu_2 rm2','rm3.id_menu_2=rm2.id_menu_2');
	  $builder->join('seguridad.rol_menu_1 rm1','rm2.id_menu_1=rm1.id_menu_1');
	  $builder->where('rm3.id_rol', $id_rol);
	  $query = $builder->get();
	  return $query;
     }
     public function getOpcionesRolMenu3Logueado($id_rol=null){
	  $builder = $this->dbconn('seguridad.rol_menu_3 rm3');
	  $builder->select
	       (
		    'm3.id_menu_2
		    ,m3.menu_3
		    ,m3.ruta'
	       );
	  $builder->join('seguridad.menu_3 m3','rm3.id_menu_3=m3.id');
	  $builder->where('rm3.id_rol', $id_rol);
	  $builder->where('m3.activo', true);
	  $builder->orderBy('m3.orden', 'asc');
	  $query = $builder->get();
	  return $query;
     }
     // Borrar De aqui Para Abajo: //
     public function getAll($estatus=null)
     {
	  $builder = $this->dbconn('seguridad.usuario as u');
	  $builder->select
	  (
	       "u.id
	       ,u.correo
	       ,u.nombre
	       ,u.apellido
	       ,r.rol
	       ,CASE WHEN u.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus
	       ,CASE WHEN u.cambiar_clave='t' THEN 'Si' ELSE 'No' END AS Reseteado"
	  );
	  $builder->join('seguridad.rol r', 'u.id_rol = r.id');
	  $builder->where('u.id_rol !=', 1);
	  if($estatus<>null)
	  {
	       $builder->where('u.activo', $estatus);
	  }
	  $query = $builder->get();
	  return $query;
     }
     public function getAllParaSistemas($estatus=null)
     {
	  $builder = $this->dbconn('seguridad.usuario as u');
	  $builder->select
	  (
	       "u.id
	       ,u.correo
	       ,u.nombre
	       ,u.apellido
	       ,r.rol
	       ,CASE WHEN u.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus
	       ,CASE WHEN u.cambiar_clave='t' THEN 'Si' ELSE 'No' END AS Reseteado"
	  );
	  $builder->join('seguridad.rol r', 'u.id_rol = r.id');
	  if($estatus<>null)
	  {
	       $builder->where('u.activo', $estatus);
	  }
	  $query = $builder->get();
	  return $query;
     }
     public function guardar($data){
	  $id_rol     =$data['id_rol'];
	  $strOpcMenu0=$data['arrMenu0'];
	  $strOpcMenu1=$data['arrMenu1'];
	  $strOpcMenu2=$data['arrMenu2'];
	  $strOpcMenu3=$data['arrMenu3'];

	  $this->db=db_connect();

	  $strSP ="Select seguridad.rol_menu_registro(";
	  $strSP.=$id_rol;
	  $strSP.=",'";
	  $strSP.=$strOpcMenu0;
	  $strSP.="','";
	  $strSP.=$strOpcMenu1;
	  $strSP.="','";
	  $strSP.=$strOpcMenu2;
	  $strSP.="','";
	  $strSP.=$strOpcMenu3;
	  $strSP.="')";
	  $query=$this->db->query($strSP)->getRow();
	  return $query;
     }
}
