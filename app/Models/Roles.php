<?php 
namespace App\Models;

class Roles extends BaseModel{

	//Metodo para obtener los roles registrados

	public function getRoles(){
		$builder = $this->dbconn('sgc_roles');
		$query = $builder->get();
		return $query;
	}
}