<?php namespace App\Controllers;

use App\Models\Roles_model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Roles extends BaseController
{
     use ResponseTrait;
     public function index()
     {
	  return ('Esta es la Página de rolexsss ...');
     }
     /*
      * Función para mostrar el listado de Roles
      */
     public function vista()
     {
	  if($this->session->get('logged'))
	  {
		   echo $this->loadTemplate('/listar_roles');
	  }
	  else
	  {
	       return redirect()->to('/');
	  }
     }
     /*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
     public function getAll()
     {
	  $model = new Roles_model();
	  if($this->session->get('id_rol')!=1)
	  {
	       $query = $model->getAll();
	  }
	  else
	  {
	       $query = $model->getAllParaSistemas();
	  }
	  if(empty($query->getResult()))
	  {
	       $roles=[];
	  }
	  else
	  {
	       $roles = $query->getResultArray();
	  }
	  echo json_encode($roles);
     }
     /*
      * Método que guarda el registro nuevo
      */
     public function save()
     {
	  $modelo = new Roles_model();
	  $data=json_decode(base64_decode($this->request->getPost('data')));
	  $datos['rol']    =$data->rol;
	  $datos['activo'] =$data->activo;
	  $query = $modelo->agregar($datos);
	  if(isset($query))
	  {
	       $mensaje=1;
	  }
	  else
	  {
	       $mensaje=0;
	  }
	  return json_encode($mensaje);
     }
     /*
      * Función para obtener los datos de un Rol
      */
     public function getDatosRol()
     {
	  if($this->request->isAJAX())
	  {
	       $data = json_decode(base64_decode($this->request->getGet('data')));
	       $datos['id']=$data->aide;
	       $modelo = new Roles_model();
	       $query = $modelo->getDatosRol($datos['id']);
	       $respuesta=[];
	       if(empty($query->getResult()))
	       {
		    $respuesta[]='0';
	       }
	       else
	       {
		    foreach($query->getResult() as $fila)
		    {
			 $respuesta['id']      =$fila->id;
			 $respuesta['rol']     =$fila->rol;
			 $respuesta['activo']  =$fila->activo;
		    }
	       }
	  }
	  else
	  {
	       redirect()->to('/403');
	  }
	  return json_encode($respuesta);
     }
     /*
      * Método que actualiza el registro
      */
     public function actualizar()
     {
	  $modelo = new Roles_model();
	  $data=json_decode(base64_decode($this->request->getPost('data')));
	  $datos['id']     =$data->aide;
	  $datos['rol']    =$data->rol;
	  $datos['activo'] =$data->activo;
	  $query = $modelo->actualizar($datos);
	  if(isset($query))
	  {
	       $mensaje=1;
	  }
	  else
	  {
	       $mensaje=0;
	  }
	  return json_encode($mensaje);
     }
}
