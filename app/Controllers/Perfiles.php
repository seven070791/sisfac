<?php namespace App\Controllers;

use App\Models\Perfiles_model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

//class Usuarios extends ResourceController
class Perfiles extends BaseController
{
     use ResponseTrait;
     public function index()
     {
	  echo ('Esta es la Página de Perfiles');
     }
     /*
      * Función para mostrar la pantalla de Perfiles
      */
     public function vista($id_rol=null, $rol=null)
     {
	  //Instancio el modelo
	  $model =new Perfiles_model();
	  //Respuesta del Controlador
	  $respuesta=[];
	  //Opciones de Menú para todos los niveles definidos
	  $strMenu0 ='';
	  $strMenu1 ='';
	  $strMenu2 ='';
	  $strMenu3 ='';
	  //Opciones de Menú para el Rol seleccionado
	  $strOptRolMenu0 ='';
	  $strOptRolMenu1 ='';
	  $strOptRolMenu2 ='';
	  $strOptRolMenu3 ='';

	  //Menu 0
	  //Opciones de Menú Nivel 0
	  $query = $model->getDatosMenu0();
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strMenu0'=>$strMenu0];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strMenu0.=$fila->id . '|' . $fila->menu_0 . '%';
	       }
	  }
	  $strMenu0=substr($strMenu0, 0, strlen($strMenu0)-1).'$';
	  //Opciones Que tiene el Rol En el Menú Nivel 0:
	  $query = $model->getOpcionesRolMenu0($id_rol);
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strOptRolMenu0'=>$strOptRolMenu0];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strOptRolMenu0.=$fila->id_menu_0 . '%';
	       }
	  }
	  $strOptRolMenu0=substr($strOptRolMenu0, 0, strlen($strOptRolMenu0)-1) . '$';

	  //Menu 1
	  $query = $model->getDatosMenu1();
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strMenu1'=>$strMenu1];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strMenu1.=$fila->id . '|' . $fila->id_menu_0 . '|' . $fila->menu_1 . '%';
	       }
	  }
	  $strMenu1=substr($strMenu1, 0, strlen($strMenu1)-1).'$';
	  //Opciones Que tiene el Rol En el Menú Nivel 1:
	  $query = $model->getOpcionesRolMenu1($id_rol);
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strOptRolMenu1'=>$strOptRolMenu1];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strOptRolMenu1.=$fila->id_menu_0 . '|' . $fila->id_menu_1 .  '%';
	       }
	  }
	  $strOptRolMenu1=substr($strOptRolMenu1, 0, strlen($strOptRolMenu1)-1) . '$';

	  // Menu 2
	  $query = $model->getDatosMenu2();
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strMenu2'=>$strMenu2];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strMenu2.=$fila->id . '|' . $fila->id_menu_1 . '|' . $fila->menu_2 . '%';
	       }
	  }
	  $strMenu2=substr($strMenu2, 0, strlen($strMenu2)-1).'$';
	  //Opciones Que tiene el Rol En el Menú Nivel 2:
	  $query = $model->getOpcionesRolMenu2($id_rol);
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strOptRolMenu2'=>$strOptRolMenu2];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strOptRolMenu2.=$fila->id_menu_0 . '|' . $fila->id_menu_1 . '|' . $fila->id_menu_2 .  '%';
	       }
	  }
	  $strOptRolMenu2=substr($strOptRolMenu2, 0, strlen($strOptRolMenu2)-1) . '$';

	  // Menu 3
	  $query    = $model->getDatosMenu3();
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strMenu3'=>$strMenu3];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strMenu3.=$fila->id . '|' . $fila->id_menu_2 . '|' . $fila->menu_3 . '%';
	       }
	  }
	  $strMenu3=substr($strMenu3, 0, strlen($strMenu3)-1).'$';
	  //Opciones Que tiene el Rol En el Menú Nivel 3:
	  $query = $model->getOpcionesRolMenu3($id_rol);
	  if(empty($query->getResult()))
	  {
	       $respuesta=['strOptRolMenu3'=>$strOptRolMenu3];
	  }
	  else
	  {
	       $registros=0;
	       foreach($query->getResult() as $fila)
	       {
		    $strOptRolMenu3.=$fila->id_menu_0 . '|' . $fila->id_menu_1 . '|' . $fila->id_menu_2 . '|' . $fila->id_menu_3 . '%';
	       }
	  }
	  $strOptRolMenu3=substr($strOptRolMenu3, 0, strlen($strOptRolMenu3)-1) . '$';

	  $respuesta=
	  [
	       'id_rol'  =>$id_rol,
	       'rol'     =>$rol,
	       'strArrayMenuOpt'=>$strMenu0 . $strMenu1 . $strMenu2 . $strMenu3,
	       'strArrayOptMenu'=>$strOptRolMenu0 . $strOptRolMenu1 . $strOptRolMenu2 . $strOptRolMenu3,
	  ];

	  if($this->session->get('logged'))
	  {
		   echo $this->loadTemplate('/vista_perfil',$respuesta);
	  }
	  else
	  {
	       return redirect()->to('/');
	  }
     }
     /*
      * Método que guarda el registro nuevo
      */
     public function guardar()
     {
	  $modelo = new Perfiles_model();

	  $datos['id_rol']   =$this->request->getPost('id_rol');
	  $datos['arrMenu0'] =$this->request->getPost('str_array_n0');
	  $datos['arrMenu1'] =$this->request->getPost('str_array_n1');
	  $datos['arrMenu2'] =$this->request->getPost('str_array_n2');
	  $datos['arrMenu3'] =$this->request->getPost('str_array_n3');
	  $query = $modelo->guardar($datos);
	  $mensaje=$query;
	  if(isset($query))
	  {
	       $mensaje=1;
	  }
	  else
	  {
	       $mensaje=0;
	  }
	  return json_encode($mensaje);
     }
	//--------------------------------------------------------------------
}
