<?php

namespace App\Controllers;

use App\Models\Casos;
use App\Models\Usuarios;
use CodeIgniter\API\ResponseTrait;

class Login extends BaseController
{
	use ResponseTrait;
	//Metodo para iniciar la sesion
	public function autenticar()
	{
		$model = new Usuarios();
		$session = session();
		$userdata = array();
		if ($this->request->isAJAX()) 
		{
			$datos = json_decode(base64_decode($this->request->getGet('data')), TRUE);
			$query = $model->obtenerUsuario($datos["username"]);
			if (isset($query)) 
			{
					 foreach ($query as $row) 
					 {
								if (password_verify($datos["userpass"], $row->clave)) 
								{
											//$userdata["nombre"] = ucwords($row->usuopnom) . ' ' . ucwords($row->usuopape);
											$userdata["nombre"]   = $row->nombre;
											$userdata["apellido"] = $row->apellido;
											$userdata["userrol"] = $row->id_rol;
											$userdata["iduser"] = $row->id;
											$userdata["activo"] = $row->activo;
											//$userdata["usuopborrado"] = $row->usuopborrado;
											//$userdata["usercargo"] = $row->usercargo;
											//if ($userdata["usuopborrado"] == 't') 
											if ($userdata["activo"] == 'f') 
											{
														 $mensaje = 0;
														 return json_encode($mensaje);
											}
											else 
											{
														 $userdata["logged"] = TRUE;
														 $session->set($userdata);
														 $mensaje = 1;
														 return json_encode($mensaje);
														 //return $this->respond(["message" => "Iniciando sesion"], 200);
											}
							 }
							 else 
							 {
								 $mensaje = 2;
								 return json_encode($mensaje);
								 //return $this->respond(["message" => "Clave incorrecta"], 401);
							 }
						 }
			} 
			else 
			{
				$mensaje = 3;
				return json_encode($mensaje);
				//return $this->respond(["message" => "Usuario o contraseña incorrectos"], 404);
			}
		} 
		else 
		{
			return redirect()->to('/');
		}
	}

	//Metodo para cerrar la sesion
	public function logout()
	{
		$this->session->destroy();
		return redirect()->to('/');
	}
}
