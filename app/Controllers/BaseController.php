<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

use App\Models\BaseModel;

use App\Models\Perfiles_model;
use App\Models\Sistema_model;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
class BaseController extends Controller
{
	/**
	 * Instance of the main Request object.
	 *
	 * @var CLIRequest|IncomingRequest
	 */
	protected $request;

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
		 protected $helpers = [];

		 /**
		 * Constructor.
		 */
		 public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
		 {
					// Do Not Edit This Line
					parent::initController($request, $response, $logger);
					// Preload any models, libraries, etc, here.
					// E.g.: $this->session = \Config\Services::session();
					$this->session = \Config\Services::session();
					//$this->cache   = \Config\Services::cache();
					//Registro del acceso
					/*
					if(strlen(base64_decode($this->request->getPost('data'))) < 1)
					{
							 $this->watson($this->request->getIPAddress(), $this->request->uri->getPath() , NULL ,$this->request->getUserAgent(), date('Y-m-d h:i:s'), NULL);
					}
					else
					{
							 $this->watson($this->request->getIPAddress(), $this->request->uri->getPath() , base64_decode($this->request->getPost('data')) ,$this->request->getUserAgent(), date('Y-m-d h:i:s'), $this->session->get('usuario'));	
					}
					 */
		 }
		 /* Metodo que permite la carga del template para los index */
		 /*Metodo para llamar la vista de un modulo*/
		 public function loadTemplate($module, $data=array())
		 {
					/* Verifico el nombre del Sistema según BD */
					$model_sistema_cnf=new Sistema_model();
					$query = $model_sistema_cnf->getNombreSistema();
					if(empty($query->getResult()))
					{
							 $data['nombreSistema']='Sin Nombre';
					}
					else
					{
							 //$data['nombreSistema']='Desde Base de Datos';
							 foreach($query->getResult() as $fila)
							 {
										$data['nombreSistema']=$fila->nombresistema;
							 }
					}
					//var_dump($respuesta);
					//var_dump($data);
					//die;

					if($module==='/login')
					{ 
							 echo view('template/header', $data);
							 echo view($module.'/content', $data);
							 echo view($module.'/footer');
					}
					else
					{
							 /* Verifico las opciones de menú que puede visualizar el Rol del usuario según BD */
							 $model_perfiles =new Perfiles_model();
							 $respuesta=[];

							 //Opciones de Menú Nivel 0
							 $query = $model_perfiles->getOpcionesRolMenu0Logueado($this->session->get('userrol'));
							 if(empty($query->getResult()))
							 {
										$matrizMenu0[]=
										[
												 'id'=>'',
												 'menu_0'=>'',
												 'ruta'  =>'',
										];
							 }
							 else
							 {
										foreach($query->getResult() as $fila)
										{
												 $matrizMenu0[]=
												 [
															'id'=>$fila->id,
															'menu_0'=>$fila->menu_0,
															'ruta'  =>$fila->ruta,
												 ];
										}
							 }
							 $respuesta['matrizMenu0']=$matrizMenu0;
							 //Opciones de Menú Nivel 1
							 $query = $model_perfiles->getOpcionesRolMenu1Logueado($this->session->get('userrol'));
							 if(empty($query->getResult()))
							 {
										$matrizMenu1[]=
										[
												 'id'       =>'',
												 'id_menu_0'=>'',
												 'menu_1'   =>'',
												 'ruta'     =>'',
										];
							 }
							 else
							 {
										foreach($query->getResult() as $fila)
										{
												 $matrizMenu1[]=
												 [
															'id'       =>$fila->id,
															'id_menu_0'=>$fila->id_menu_0,
															'menu_1'   =>$fila->menu_1,
															'ruta'     =>$fila->ruta,
												 ];
										}
							 }
							 $respuesta['matrizMenu1']=$matrizMenu1;
							 $query = $model_perfiles->getOpcionesRolMenu2Logueado($this->session->get('userrol'));
							 if(empty($query->getResult()))
							 {
										$matrizMenu2[]=
										[
											'id'       =>'',
											'id_menu_1'=>'',
											'menu_2'   =>'',
											'ruta'     =>'',
										];
							 }
							 else
							 {
										foreach($query->getResult() as $fila)
										{
												 $matrizMenu2[]=
												 [
															'id'       =>$fila->id,
															'id_menu_1'=>$fila->id_menu_1,
															'menu_2'   =>$fila->menu_2,
															'ruta'     =>$fila->ruta,
												 ];
										}
							 }
							 $respuesta['matrizMenu2']=$matrizMenu2;
							 //Opciones de Menú Nivel 3
							 $query = $model_perfiles->getOpcionesRolMenu3Logueado($this->session->get('userrol'));
							 if(empty($query->getResult()))
							 {
										$matrizMenu3[]=
										[
												 'id_menu_2'=>'',
												 'menu_3'   =>'',
												 'ruta'     =>'',
										];
							 }
							 else
							 {
										foreach($query->getResult() as $fila)
										{
												 $matrizMenu3[]=
												 [
															'id_menu_2'=>$fila->id_menu_2,
															'menu_3'   =>$fila->menu_3,
															'ruta'     =>$fila->ruta,
												 ];
										}
							 }
							 //var_dump($respuesta);
							 //die;
							 $respuesta['matrizMenu3']=$matrizMenu3;
							 echo view('template/header',$data);
							 //echo view('template/nav_bar',$data);
							 echo view('template/nav_bar',$respuesta);
							 echo view($module . '/content');
							 echo view('template/footer');
							 echo view($module . '/footer');
					}
		 }
		 /**
		 * Metodo que permite registrar los movimientos de la app
		 * por parte del usuario, esto permite un control mas exhaustivo
		 * de la app
		 *
		 * @var access_ip : String => La IP de acceso del usuario
		 * @var url_request : String => El recurso (Controlador) que intenta acceder
		 * @var data_request : String => Datos Via JSON (Envio de formularios) en caso de darse, puede ser NULL
		 * @var user_agent : String => El navegador que utiliza el usuario
		 * @var access_date: TIMESTAMP => Fecha y hora de acceso
		 * @var user_access: String => Usuario que solicita el recurso
		 */
		 public function watson(String $access_ip, String $url_request, String $data_request = NULL, String $user_agent, $access_date, String $user_access = NULL)
		 {
					$model = new BaseModel();
					$query = $model->recordLog(array(
					'access_ip' => $access_ip,
					'url_request' => $url_request,
					'data_request' => $data_request,
					'user_agent' => $user_agent,
					'access_date'=> $access_date,
					'user_access'=> $this->session->get('usuario')
					));
					if($query)
					{
							 return TRUE;
					}
					else
					{
							 return FALSE;
					}
		 }


	/*Funcion que formatea fechas*/
	public function formatearFecha($fecha)
	{
		$date1 = explode('-', $fecha);
		$date2 = $date1[2] . "-" . $date1[1] . "-" . $date1[0];
		return $date2;
	}
	/**
	 * Metodo que permite generar tablas debidamente formateadas
	 * usando Bootstrap4
	 * 
	 *
	 * @param heading : Array  => Arreglo con las cabeceras del usuario
	 * @param data    : Array   => Los datos de la tabla puestos en un arreglo de arreglos
	 * 
	 */
}
