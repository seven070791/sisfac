<?php

namespace App\Controllers;

use App\Models\Casos;

class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}
	//--------------------------------------------------------------------
	// //Todas las vistas deben ser cargadas aqui
	public function login()
	{
		 echo $this->loadTemplate('/login');
		 /*
		echo view('template/header');
		echo view('login/content');
		echo view('login/footer');
			*/
	}
	//Vista principal
	public function dashboard()
	{
		if ($this->session->get('logged')) {
		 echo $this->loadTemplate('/dashboard');
		 /*
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('dashboard/content');
			echo view('template/footer');
			echo view('dashboard/footer');
			*/
		} else {
			return redirect()->to('/');
		}
	}

	//Vista principal
	public function pantalla_bienvenida()
	{
		if ($this->session->get('logged')) {
		 echo $this->loadTemplate('/pantalla_bienvenida');
			//Pasamos la tabla como parametro para la vista
				 /*
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('pantalla_bienvenida/content.php');
			echo view('template/footer');
			echo view('pantalla_bienvenida/footer_bienvenida.php');
					*/
		} else {
			return redirect()->to('/');
		}
	}
}
